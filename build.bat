@echo off
SetLocal

set DST=usr/lib/enigma2/python/Plugins/Extensions/MovieJoin/
set "TAR=c:\cygwin\bin\tar --format=gnu"
set PYTHON=c:\Language\Python27\python

%PYTHON% -O -m compileall -d /%DST% __init__.py
if errorlevel 1 goto :eof
%PYTHON% -O -m compileall -d /%DST% plugin.py
if errorlevel 1 goto :eof

set tmp_dir=IPKG_BUILD.$$
mkdir %tmp_dir%

%TAR% -czf %tmp_dir%/data.tar.gz --no-recur --transform s:.:%DST%: . ./*.pyo
%TAR% -czf %tmp_dir%/control.tar.gz ./control

echo 2.0> %tmp_dir%\debian-binary
dtou %tmp_dir%\debian-binary

for /f "tokens=1,2" %%U in (control) do if %%U==Version: set version=%%V
set pkg_file=enigma2-plugin-extensions-moviejoin_%version%_all.ipk

del %pkg_file% 2>nul
%TAR% -czf %pkg_file% -C %tmp_dir% ./debian-binary ./control.tar.gz ./data.tar.gz

rmdir /q/s %tmp_dir%

echo Created %pkg_file%
