from Plugins.Plugin import PluginDescriptor
from Screens.MessageBox import MessageBox
from Screens.MovieSelection import MovieSelection
from enigma import eServiceCenter
from Tools.Directories import fileExists
from Components.Task import PythonTask, Job, job_manager as JobManager
from time import sleep
from shutil import copyfile
import os, glob, struct

apscParser = struct.Struct('>qq')   # big-endian, 64-bit offset and data
cutsParser = struct.Struct('>qi')   # big-endian, 64-bit PTS and 32-bit type

def Plugins(**kwargs):
    return PluginDescriptor(
        name="MovieJoin",
        description=_("Join..."),
        where=PluginDescriptor.WHERE_MOVIELIST,
        multi=True,
        fnc=main)

def main(session, service, serviceList=None, **kwargs):
    session.open(MovieJoin, service, serviceList)

class MovieJoin(MessageBox):
    def __init__(self, session, service, serviceList):
        path = service.getPath()
        if not path.endswith('.ts'):
            MessageBox.__init__(self, session, _('"%s" is not joinable.') % os.path.basename(path), type=MessageBox.TYPE_ERROR, timeout=10)
            return
        if serviceList:
            self.fileList = []
            for item in serviceList[:]:
                if item.getPath().endswith('.ts'):
                    self.fileList += [item.getPath()]
                else:
                    serviceList.remove(item)
        else:
            self.fileList = self.getFiles(path)
        self.setName(service)
        if len(self.fileList) <= 1:
            MessageBox.__init__(self, session, _('"%s" has nothing to join.') % self.name, type=MessageBox.TYPE_INFO, timeout=10)
        else:
            self.serviceList = serviceList
            self.meta = 0
            tlist = [(_("no"), False),
                     (_("yes - delete parts"), "CALLFUNC", self.join, "delete"),
                     (_("yes - keep parts"), "CALLFUNC", self.join, "keep"),
                     (_("yes - new movie"), "CALLFUNC", self.join, "new")]
            self.files = "\n"
            for f in self.fileList:
                self.files += "\n"
                if serviceList:
                    name = os.path.basename(f)[:-3]
                    if len(name) > 45:
                        name = name[:32] + "..." + name[-10:]
                    self.files += name
                else:
                    self.files += self.name
                    if f[-7] == '_':
                        self.files += " " + f[-6:-3]
            MessageBox.__init__(self, session, _('Join "%s"?') % self.name + self.files, list=tlist)

    def setName(self, service):
        serviceHandler = eServiceCenter.getInstance()
        info = serviceHandler.info(service)
        if not info:
            self.name = service.getPath()
        else:
            self.name = info.getName(service)

    def left(self):
        if self.serviceList:
            self.meta -= 1
            if self.meta < 0:
                self.meta = len(self.serviceList) - 1
            self.updateText()
        else:
            super(MovieJoin, self).left()

    def right(self):
        if self.serviceList:
            self.meta += 1
            if self.meta >= len(self.serviceList):
                self.meta = 0
            self.updateText()
        else:
            super(MovieJoin, self).right()

    def updateText(self):
        self.setName(self.serviceList[self.meta])
        self["text"].setText(_('Join "%s"?') % self.name + self.files)

    def getFiles(self, path):
        # Assume "*_???.ts" is a numbered movie.
        primary = (path[:-7] + '.ts') if path[-7] == '_' else path
        secondary = glob.glob(primary[:-3] + '_???.ts')
        secondary.sort()
        if fileExists(primary):
           return [primary] + secondary
        return secondary

    def join(self, arg):
        job = Job(_("Joining movie"))
        if arg == "new":
            self.name += " joined"
        JoinTask(job, self.session, self.name, self.meta, self.fileList, arg)
        JobManager.AddJob(job)
        self.close()

class JoinTask(PythonTask):
    def __init__(self, job, session, name, meta, fileList, how):
        PythonTask.__init__(self, job, name)
        self.session = session
        self.name = name
        self.meta = fileList[meta]
        self.main = fileList[0]
        self.parts = fileList[1:]
        self.how = how

    def prepare(self):
        self.end = 0
        self.main_size = os.path.getsize(self.main)
        if self.how == "new":
            f = self.main
            for ff in (f, f + '.ap', f + '.sc', f + '.cuts', f + '.meta', f[:-3] + '.eit'):
                try: self.end += os.path.getsize(ff)
                except: pass
        for f in self.parts:
            self.end += os.path.getsize(f)
            try: self.end += os.path.getsize(f + '.ap') * 1000
            except: pass
            try: self.end += os.path.getsize(f + '.sc') * 100
            except: pass

    def work(self):
        # The thread may be created before the timer.
        while not hasattr(self, 'timer'):
            sleep(0.01)
        self.timer.changeInterval(1000)     # every second, not 5ms
        if self.how == "new":
            for ext in ('', '.ap', '.sc', '.cuts', '.meta', '.eit'):
                self.copyFile(ext)
                if self.aborted:
                    self.main = self.meta[:-3] + '_joined.ts'
                    return "Joining aborted."
            self.main = self.meta[:-3] + '_joined.ts'
        with open(self.main, 'ab') as movie:
            for f in self.parts:
                with open(f, 'rb') as part:
                    while True:
                        data = part.read(128*1024)
                        if not data: break
                        if self.aborted:
                            return "Joining aborted."
                        movie.write(data)
                        self.pos += len(data)
        self.joinCuts()
        self.joinAPSC('.ap', apscParser.size * 1000)
        self.joinAPSC('.sc', apscParser.size * 100)

        if self.how == "delete":
            if self.meta != self.main:
                for ext in ('', '.ap', '.sc', '.cuts'):
                    try: os.rename(self.main + ext, self.meta + ext)
                    except: pass
                try: os.remove(self.main + '.meta')
                except: pass
                try: os.remove(self.main[:-3] + '.eit')
                except: pass
                self.parts.remove(self.meta)
            for f in self.parts:
                for ff in (f, f + '.ap', f + '.sc', f + '.cuts', f + '.meta', f[:-3] + '.eit'):
                    try: os.remove(ff)
                    except: pass
        elif self.how == "keep":
            if self.meta != self.main:
                try: copyfile(self.meta + '.meta', self.main + '.meta')
                except: pass
                try: copyfile(self.meta[:-3] + '.eit', self.main[:-3] + '.eit')
                except: pass
        else: #self.how == "new":
            try:
                with open(self.meta + '.meta', 'rb') as old, open(self.main + '.meta', 'wb') as new:
                    meta = old.readlines()
                    meta[1] = meta[1][:-1] + ' joined\n'
                    new.write(''.join(meta))
            except:
                pass
            # .eit is done directly in copyFile.

        self.refresh = lambda: None
        if isinstance(self.session.current_dialog, MovieSelection):
            if self.how in ("delete", "new") or self.meta != self.main:
                self.refresh = self.session.current_dialog.reloadList

    def copyFile(self, ext):
        if ext == '.eit':
            oldname = self.meta[:-3] + '.eit'   # no, it's not main
            newname = self.meta[:-3] + '_joined.eit'
        else:
            oldname = self.main + ext
            newname = self.meta[:-3] + '_joined.ts' + ext
        if not fileExists(oldname):
            return
        with open(oldname, 'rb') as old, open(newname, 'wb') as new:
            while not self.aborted:
                data = old.read(128*1024)
                if not data: break
                new.write(data)
                self.pos += len(data)

    def joinAPSC(self, suffix, pos_size):
        try:
            size = self.main_size
            with open(self.main + suffix, 'ab') as movie:
                for f in self.parts:
                    with open(f + suffix, 'rb') as part:
                        while True:
                            data = part.read(apscParser.size)
                            if len(data) < apscParser.size:
                                break
                            ofs, data = apscParser.unpack(data)
                            ofs += size
                            movie.write(apscParser.pack(ofs, data))
                            self.pos += pos_size
                    size += os.path.getsize(f)
        except:
            pass

    def joinCuts(self):
        try:
            length = self.getLengthFromMeta(self.main)
            # In-in combinations take the last in, not the first, so we can't
            # just make an implicit in explicit.
            lastCut = 0
            with open(self.main + '.cuts', 'rb') as movie:
                while True:
                    data = movie.read(cutsParser.size)
                    if len(data) < cutsParser.size:
                        break
                    pts, data = cutsParser.unpack(data)
                    if data in (0, 1): # in/out cuts
                        lastCut = data
            with open(self.main + '.cuts', 'ab') as movie:
                for f in self.parts:
                    with open(f + '.cuts', 'rb') as part:
                        firstCut = True
                        while True:
                            data = part.read(cutsParser.size)
                            if len(data) < cutsParser.size:
                                break
                            pts, data = cutsParser.unpack(data)
                            if data != 3: # ignore resume point
                                # Need to make the implicit explicit.
                                if data in (0, 1):
                                    if firstCut:
                                        firstCut = False
                                        if lastCut == data:
                                            movie.write(cutsParser.pack(length, 1 - data))
                                    lastCut = data
                                pts += length
                                movie.write(cutsParser.pack(pts, data))
                    length += self.getLengthFromMeta(f)
        except:
            pass

    def getLengthFromMeta(self, name):
        with open(name + '.meta') as meta:
            for x in range(5):
                meta.readline()
            length = int(meta.readline())
            size = int(meta.readline())
        fsize = self.main_size if name == self.main else os.path.getsize(name)
        if size != fsize:
            raise ValueError
        return length

    def cleanup(self, failed):
        if failed:
            if self.how == "new":
                f = self.main
                for ff in (f, f + '.ap', f + '.sc', f + '.cuts', '.meta', f[:-3] + '.eit'):
                    try: os.remove(ff)
                    except: pass
            else:
                with open(self.main, 'r+b') as f:
                    f.truncate(self.main_size)
        else:
            self.setProgress(self.end)
            self.session.open(MessageBox, _('Joined "%s".') % self.name, MessageBox.TYPE_INFO, timeout=10)
            self.refresh()
